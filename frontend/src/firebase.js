// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";

const firebaseConfig={
apiKey: "AIzaSyBBoJLl6J2UCxSurTS8PQNHSiMfcw59kJM",
authDomain: "peer-code-review.firebaseapp.com",
projectId: "peer-code-review",
storageBucket: "peer-code-review.appspot.com",
messagingSenderId: "254864561871",
appId: "1:254864561871:web:d9e0b4628977a2956c49ba",
measurementId: "G-9QX3HQ41E9"};

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();
const provider = new firebase.auth.GoogleAuthProvider();

export {db, auth, storage, provider};
