import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import reducer, { initialState } from './reducer';
import registerServiceWorker from './registerServiceWorker';
import { StateProvider } from './StateProvider';

const baseUrl = ""
const rootElement = document.getElementById('root');

ReactDOM.render(
  <BrowserRouter basename={baseUrl}>
    <StateProvider initialState={initialState} reducer={reducer}>
      <App />
    </StateProvider>
  </BrowserRouter>,
  rootElement);

registerServiceWorker();