import React, { useState, useEffect } from 'react';
import './css/App.css';
import Login from './components/Login'
import { Layout } from './components/Layout';
import { useStateValue } from "./StateProvider"



function App() {
  const [{ user }, dispatch] = useStateValue();
  return (
    <div className='app'>
      {!user ? (<Login />) :
        (
          <>
            <h1>Code Peer Review</h1>
            <Layout></Layout>
          </>
        )}
    </div>
  );
}

export default App;
  //test



