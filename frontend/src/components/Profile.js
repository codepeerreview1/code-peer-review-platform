import axios from "axios"
import React, { useState } from 'react'
import '../css/Profile.css'
import csvToJson from 'convert-csv-to-json'
import CSVReader from 'react-csv-reader'
import { auth } from "../firebase"
import { BrowserRouter as Router, Route, NavLink, Switch, Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';
import Login from './Login';


class Profile extends React.Component {


    static displayName = Profile.name;


    constructor(props) {
        super(props);
        this.state = { email: '',user: {}, student: [], loading: true };
        if (auth.currentUser === null) {
            window.alert("Login First");
            this.props.history.push('/Login');
            <Router>
            <Redirect to="/Login" />
            <Route path="/Login" component={Login} />
        </Router>
}else{
     this.state.email = auth.currentUser.email 
}
    }

    async componentDidMount() {
        axios.get("http://localhost:8099/api/users/id/" + this.state.email + "/profile").then((response) => {
            this.setState({
                user: response.data
            })
            console.log(this.state.user)
        });
    }

    changeHandler = (e) => {
        console.log(e.target.value);
        let json = csvToJson.getJsonFromCsv(e.target.value);
        console.log(json);
    }

    handleCSV = (data) => {
        //this.state.setCSV(data)

        data = this.csvJSON(data)
        console.log(data);
        axios.post("http://localhost:8099/api/courses/create", data).then(response => {
            this.state.student = response.data
            console.log(response)
            this.props.history.push("/profile");
        })
            .catch(error => {
                console.log(error)
            })

        this.setState({ student: data })
        console.log(this.state.student);
    }

    csvJSON(data) {

        var arrayObj = [];
        var header = []
        for (var j = 0; j < data.length; j++) {
            if (j === 0) {
                for (let i = 0; i < data[0].length; i++) {
                    header.push(data[0][i])
                }
            }
            else {
                let obj = {}
                for (let i = 1; i < data[j].length; i++) {
                    obj[header[i]] = data[j][i];
                }
                arrayObj.push(obj)
            }

        }
        console.log(arrayObj)
        return arrayObj;
        //return JSON.stringify(obj);
    }

    render() {
        return (
            <div>
                <div className="profile">
                    <div className="profile_top">
                        <div className="profile_left">
                            <div className="profile_item profile_name">Name: {this.state.user.first} {this.state.user.last}</div>
                            <div className="profile_item profile_course">Course: {this.state.user.course}</div>
                            <div className="profile_item profile_email">Email address: {this.state.user.email}</div>
                        </div>
                        {this.state.user.type === "instructor" && (
                        <div className="profile_right">
                            <div className="profile_item profile_upload">
                                <div>Upload CSV Roster:</div>
                                <CSVReader onFileLoaded={this.handleCSV}></CSVReader>

                            </div>

                        </div>)}

                    </div>
                </div>
            </div>
        )
    }
}



export default Profile

//to push into gitlab:
//yes