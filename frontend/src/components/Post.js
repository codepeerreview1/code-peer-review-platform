import React, { Component } from 'react';
import PostService from '../PostService';
import '../css/Post.css'
import axios from 'axios'
import { BrowserRouter as Router, Route, NavLink, Switch, Link } from "react-router-dom";
import { auth } from "../firebase";
import { Redirect } from 'react-router-dom';
import Login from './Login';

function showAlert(e) {
    e.preventDefault();
        var myText = "Reported";
        alert (myText);
      }

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}


class Post extends Component {

    static displayName = Post.name;



    constructor(props) {
        super(props);
            this.state = { posts: [], loading: true };
            if (auth.currentUser === null) {
                window.alert("Login First");
                this.props.history.push('/Login');
                <Router>
                <Redirect to="/Login" />
                <Route path="/Login" component={Login} />
            </Router>
    }
}

    async componentDidMount() {
        PostService.getNonFlaggedPosts().then((response) => {
            this.setState({ posts: response.data })
        });
    }

    submitFlagged = (data) => {
        axios.put("http://localhost:8099/api/posts/report/" + data);
    }



    render() {
            return (
            <div>
                {this.state.posts.map(myPost =>
                    <div className='post' key={myPost.id}>
                        <div className='Post_top'>
                            <h5 className="username">{myPost.user}</h5>
                            <h4>
                                <Link to={`/home/${myPost.url}`}>
                                    {myPost.title}
                                </Link>
                            </h4>
                            <form onSubmit={this.submitFlagged(myPost.id)}>
                            <button className='Report_btn' onClick={showAlert} >Report</button>
                            </form>
                        </div>
                        <h5 className='Main_text'>{myPost.content}</h5>
                    </div>
                )}
            </div>
        );
    }
        
    }



export default Post