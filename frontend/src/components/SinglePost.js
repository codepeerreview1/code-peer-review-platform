import React, { Component, useState, useEffect } from 'react';
import PostService from '../PostService';
import '../css//singlePost.css'
import axios from 'axios'
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { useHistory } from 'react-router-dom';
import { auth } from "../firebase";

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
function showAlert(e) {
    e.preventDefault();
    var myText = "Reported";
    alert(myText);
}
class SinglePost extends Component {

    static displayName = SinglePost.name;


    constructor(props) {
        super(props);
        this.state = {
            postInfo: [],
            comments: [],
            loading: true,
            lineNumber: "",
            lineComments: "",
            user: auth.currentUser.email,
        };
    }
    changeHandler = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    async componentDidMount() {
        const { match: { params } } = this.props;

        axios.get(`http://localhost:8099/api/posts/id/${params.id}`).then((response) => {
            console.log(response);
            this.setState({ postInfo: response.data });
        });

        axios.get(`http://localhost:8099/api/comments/post/${params.id}`).then((response) => {
            console.log(response);
            this.setState({ comments: response.data });

        });
    }

    submitFlagged = (data) => {
        axios.put("http://localhost:8099/api/posts/report/" + data);
    }

    submitHandler = (e) => {
        e.preventDefault()

        //build json
        const commentJson = {

            lineNums: this.state.lineNumber,
            lineComments: this.state.lineComments,
            user: this.state.user

        };
        const { match: { params } } = this.props;
        axios.post(`http://localhost:8099/api/comments/${params.id}`, commentJson)
            .then(response => {
                console.log(response)
                this.props.history.push("/home");
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        return (
            <div>
                {this.state.postInfo.map(myPost =>
                    <div className='Singlepost' key={myPost.id}>
                        <div className='Post_top'>
                            <h5 className="username">{myPost.user}</h5>
                            <h4>
                                {myPost.title}
                            </h4>
                            <form onSubmit={this.submitFlagged(myPost.id)}>
                                <button className='Report_btn' onClick={showAlert} >Report</button>
                            </form>

                        </div>
                        <h5 className='Main_text'>{myPost.content}</h5>
                        <p className="codes"><SyntaxHighlighter language="java" style={docco}>{myPost.code}</SyntaxHighlighter></p>
                        <div>
                            <button className='cmd_btn' onClick={myFunction}>Comment</button>
                            <div id="myDropdown" className="dropdown-content">
                                <div className='a'>

                                    <form onSubmit={this.submitHandler}>
                                        <div className="wholeComment">
                                            <div className="Commentcontents">
                                                <h5 className="username">{this.state.user}</h5>
                                                <textarea type="text" placeholder='Your comment here....' name="lineComments" onChange={this.changeHandler} />
                                            </div>
                                            <div className='comment_btn'>
                                                <button className="Commentupload" type="submit">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                )}
                <div>
                    <div className='Comment_section'>
                        <h5>Comments</h5>
                        {this.state.comments.map(comment =>

                            <div className='comment' key={comment.id}>
                                <div className='Comment_top'>
                                    <div className='name_and_report'><h5 className="Commentuser">{comment.user}</h5>
                                        <form onSubmit={this.submitFlagged(comment.id)}>
                                            <button className='Report_btn_cmt' onClick={showAlert} >Report This Comment</button>
                                        </form>
                                    </div>
                                    <p className="Main_text_cmt">{comment.lineComments}</p>

                                </div>
                            </div>

                        )}
                    </div>
                </div>
            </div>
        );
    }



}



export default SinglePost;