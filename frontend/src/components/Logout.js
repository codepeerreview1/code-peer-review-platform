import React from 'react'
import { useHistory } from 'react-router-dom';
import { auth } from '../firebase'
import { Redirect } from 'react-router-dom';


function Logout() {
  // const [{user},dispatch] = useStateValue();
  const history = useHistory('');

  if (auth.currentUser === false) {
    <Redirect to="/Login" />
    history.push("/Login")
  }

  const logout = (event) => {
    if (auth.currentUser === true) {
      auth.currentUser = null;
    }
    event.preventDefault();
    auth.signOut();
    if (auth.currentUser === null) {
      history.push("/Login")
    }
  }


  return (
    <div>
      <button className='logout' onClick={logout}> Logout </button>
    </div>
  )
}

export default Logout
