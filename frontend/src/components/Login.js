import React from 'react'
import '../css/Login.css'
import { Button } from "@material-ui/core"
import { auth, provider } from "../firebase"
import { actionTypes } from "../reducer"
import { useStateValue } from "../StateProvider"
import { useHistory } from 'react-router-dom';


function Login() {
    const history = useHistory('');
    if (auth.currentUser === true) {
        auth.currentUser = null
    }

    const [state, dispatch] = useStateValue();

    const signIn = () => {
        auth.signInWithPopup(provider)
            .then((result) => {
                dispatch({
                    type: actionTypes.SET_USER,
                    user: result.user,
                })
                console.log(result);
                history.push("/home")
            })
            .catch((error) => alert(error.message))


    }


    return (
        <div className='login'>
            <div className='container1'>
                <h3> SUNY Oswego Java Code Peer Review Platform</h3>
                <div className='button'>
                    <Button type='submit' onClick={signIn}> Sign In </Button>
                </div>

            </div>

        </div>
    );
}

export default Login
