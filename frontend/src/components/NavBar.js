import React, { Component } from 'react';
import '../css/NavBar.css'
import { BrowserRouter as Router, Route, NavLink, Switch } from "react-router-dom";
import PostCreation from "./PostCreation";
import Logout from './Logout'
import Login from './Login';
import Profile from './Profile';
import Post from './Post';
import SinglePost from './SinglePost'



export class NavBar extends Component {
    static displayName = NavBar.name;

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (
            <Router>
                <div className='NavBar'>
                    <div className="nav_header">Dashboard</div>
                    <div className="nav_item" ><NavLink exact activeClassName="active" to="/home" >Home</NavLink></div>
                    <div className="nav_item" ><NavLink exact activeClassName="active" to="/make_a_post" >Make a Post</NavLink></div>
                    <div className="nav_item" ><NavLink exact activeClassName="active" to="/profile" >View Profile</NavLink></div>
                    <Logout />
                </div>

                <Switch>
                    <Route exact path="/home" component={Post} />
                    <Route exact path="/make_a_post" component={PostCreation} />
                    <Route exact path="/profile" component={Profile} />
                    <Route exact path="/Login" component={Login} />
                    <Route path="/home/:id" component={SinglePost} />
                    <Route path="/" component={Post} />
                </Switch>
            </Router>

        );
    }
}