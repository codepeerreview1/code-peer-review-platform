import React, { useState, Component } from "react";
import "../css/PostCreation.css";
import axios from "axios";
import { auth } from "../firebase";
import { BrowserRouter as Router, Route, NavLink, Switch, Link } from "react-router-dom";
import { Redirect } from 'react-router-dom';
import Login from './Login';

class PostCreation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: '',
      code: '',
      file: '',
      user: '',
    };
    if (auth.currentUser === null) {
      window.alert("Login First");
      this.props.history.push('/Login');
      <Router>
      <Redirect to="/Login" />
      <Route path="/Login" component={Login} />
  </Router>
}else{
  this.state.user=auth.currentUser.email
}
  }

  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  //when file is uploaded, it will call this function
  showFile = async (e) => {
    e.preventDefault();
    const reader = new FileReader(); //FileReader object lets we apps asynchronously read the contents of files stored in the user's computer
    e.preventDefault();
    reader.onload = async (e) => {
      const text = e.target.result;
      this.setState({ file: text });
    };
    reader.readAsText(e.target.files[0]);
  };

  submitHandler = (e) => {
    e.preventDefault();
    console.log(this.state);
    axios
      .post("http://localhost:8099/api/posts/create", this.state)
      .then((response) => {
        console.log(response);
        this.props.history.push("/home");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    const { title, content, code, file, user } = this.state;
    return (
      <form onSubmit={this.submitHandler}>
        <div className="whole">
          <div className="post_top">
            <h5 className="user">{user}</h5>
            <div className="title">
              <input
                type="text"
                placeholder="Enter the title"
                name="title"
                value={title}
                required="true"
                onChange={this.changeHandler}
              />
            </div>
          </div>
          <div className="contents">
            <div className="content_area">
              <input
                type="text"
                placeholder="Text Content"
                name="content"
                value={content}
                required="true"
                onChange={this.changeHandler}
              />
            </div>
            <textarea
              type="text"
              placeholder="codes...."
              name="code"
              value={code}
              onChange={this.changeHandler}
            />
          </div>
          <input className="file" type="file" onChange={this.showFile} />
          <button className="upload" type="submit">Submit</button>
        </div>
      </form>
    );
  }
}

export default PostCreation;
