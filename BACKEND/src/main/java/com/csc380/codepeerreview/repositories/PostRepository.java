package com.csc380.codepeerreview.repositories;

import java.util.List;

import com.csc380.codepeerreview.models.Post;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends MongoRepository<Post, String> {

    List<Post> findByFlagged(boolean b);

    List<Post> findByUser(String user);

    Post findByUserAndUrl(String user, String pathUrl);

    List<Post> findByUrl(String url);

    Post findById(ObjectId id);

    Post deleteById(ObjectId id);

}
