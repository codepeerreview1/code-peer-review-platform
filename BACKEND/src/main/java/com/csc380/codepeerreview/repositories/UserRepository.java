package com.csc380.codepeerreview.repositories;

import java.util.List;

import com.csc380.codepeerreview.models.User;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, Long> {

    public User findById(ObjectId id);  

    public User findByEmail(String email);

    public List<User> findByCourse(String course);

    public List<User> findByCourseAndType(String course, String type);

    public List<User> findByType(String type);

    public void deleteByType(String type);

    public void deleteById(ObjectId id);

}
