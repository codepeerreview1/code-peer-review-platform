package com.csc380.codepeerreview.repositories;

import java.util.Optional;

import com.csc380.codepeerreview.models.Gradebook;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradebookRepository extends MongoRepository<Gradebook, String>{

    Gradebook findByCourseID(String courseID);

    Gradebook findById(ObjectId id);

    Gradebook deleteById(ObjectId id);

    Gradebook deleteByCourseID(String courseID);

}
