package com.csc380.codepeerreview.repositories;

import org.springframework.stereotype.Repository;

import java.util.List;

import com.csc380.codepeerreview.models.CommentSubmission;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

@Repository
public interface CommentSubmissionRepository extends MongoRepository<CommentSubmission, Long> {

    public List<CommentSubmission> findByUrl(String url);

    public List<CommentSubmission> findByFlagged(boolean flagged);

    public List<CommentSubmission> findByUser(String user);

    public List<CommentSubmission> findByHelpful(boolean helpful);

    public List<CommentSubmission> findByUserAndHelpful(String user, boolean helpful);

    public List<CommentSubmission> findByPost(ObjectId post);

    public CommentSubmission findById(ObjectId id);

    public CommentSubmission deleteById(String id);

    public List<CommentSubmission> findByUserAndFlagged(String user, boolean flagged);

}
