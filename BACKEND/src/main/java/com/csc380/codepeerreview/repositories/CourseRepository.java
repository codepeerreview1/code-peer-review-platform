package com.csc380.codepeerreview.repositories;

import com.csc380.codepeerreview.models.Course;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {

    Course findByName(String name);

}
