package com.csc380.codepeerreview.controllers;

import java.util.HashMap;
import java.util.List;

import com.csc380.codepeerreview.models.Gradebook;
import com.csc380.codepeerreview.models.User;
import com.csc380.codepeerreview.repositories.GradebookRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class GradebookController {

    // used to work a repository with a controller
    @Autowired
    public GradebookRepository gradebookRepository;

    // Find all gradebooks
    @GetMapping(value = "/gradebooks")
    public List<Gradebook> getAllGradebook() {
        return gradebookRepository.findAll();
    }

    // Find by course
    @GetMapping(value = "/gradebooks/{courseID}")
    public Gradebook getGradebook(@PathVariable String courseID) {
        Gradebook desiredGradebook = gradebookRepository.findByCourseID(courseID);
        return desiredGradebook;
    }

    // Delete by ID
    @DeleteMapping(value = "/gradebooks/{id}")
    public String deleteGradebookById(@PathVariable ObjectId id) {
        Gradebook gradebook = gradebookRepository.findById(id);

        if (gradebook != null) {
            gradebookRepository.deleteById(id);
            return "Gradebook deleted.";
        } else {
            return "Unable to find gradebook. Try again.";
        }
    }

    // Delete by Course
    @DeleteMapping(value = "/gradebooks/{courseId}")
    public String deleteGradebookById(@PathVariable String courseID) {
        Gradebook gradebook = gradebookRepository.findByCourseID(courseID);

        if (gradebook != null) {
            gradebookRepository.deleteByCourseID(courseID);
            return "Gradebook deleted.";
        } else {
            return "Unable to find gradebook. Try again.";
        }
    }

    // Delete All
    @DeleteMapping(value = "/gradebooks/deleteAllGradebooks")
    public String deleteAllGradebooks() {
        gradebookRepository.deleteAll();
        return "All gradebooks deleted";
    }

    // Create our gradebook
    @PostMapping(value = "/gradebooks/create")
    public Gradebook createGradebook(@RequestBody User[] students) {

        
        String courseID = "";
        boolean gotCourseID = false;
        HashMap <String, String> grades= new HashMap<String, String> ();
        Gradebook g = new Gradebook();

        for (User s : students) {
            String properName = s.getNameFirst() + " " + s.getNameLast();
            grades.put(properName, "Ungraded");
            if(!gotCourseID){
                courseID = s.getCourse();
                gotCourseID = true;
            }
        }

        g.setGrades(grades);
        g.setCourseID(courseID);
        g.setStudents(students);

        gradebookRepository.insert(g);
        return g;
    }

    // Update Grade for a student
    //Get the course
    //Get the gradebook in the course
    //Find the desired User
    //edit the value
    @PutMapping(value = "/gradebooks/{courseID}")
    public String editGrade(@PathVariable String courseID, String name, String grade) {
        Gradebook gradebook = gradebookRepository.findByCourseID(courseID);
        gradebook.enterGrade(name, grade);
        gradebookRepository.save(gradebook);
        return "The grade " + grade + " was given to " + name;
    }

}