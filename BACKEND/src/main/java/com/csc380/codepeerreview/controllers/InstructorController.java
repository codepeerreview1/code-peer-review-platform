package com.csc380.codepeerreview.controllers;

import java.util.List;

import com.csc380.codepeerreview.models.User;
import com.csc380.codepeerreview.repositories.UserRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InstructorController {

    @Autowired
    public UserRepository userRepo;

    @GetMapping(value = "/instructors")
    public List<User> getAllInstructors() {

        return userRepo.findByType("instructor");
    }

    @PostMapping(value = "/instructors")
    public User createinstructor(@RequestBody User instructor) {

        instructor.setType("instructor");
        instructor.makeUsername(instructor.getEmail());
        User insertedInstructor = userRepo.insert(instructor);

        return insertedInstructor;
    }

    // Delete all instructors
    @DeleteMapping(value = "/instructors/deleteAllInstructors")
    public String deleteAllInstructors() {
        userRepo.deleteAll();
        return "All data deleted";
    }

    // Delete particular Instructors
    @DeleteMapping(value = "/instructors/{i}")
    public String deleteInstructor(@PathVariable String id) {
        User in = userRepo.findById(new ObjectId(id));
        if (in != null) {
            userRepo.deleteById(new ObjectId(id));
            return "Instructor deleted. _id = " + id;
        } else
            return "Instructor not found. Please try again.";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public ResourceNotFoundException() {
        }
    }

    @PutMapping(value = "/instructors/{id}")
    public User editInstructor(@PathVariable ObjectId id, @RequestBody User instructor) {
        User editedInstructor = userRepo.findById(id);

        editedInstructor.setNameLast(instructor.getNameLast());
        editedInstructor.setNameFirst(instructor.getNameFirst());
        editedInstructor.setSchoolID(instructor.getSchoolID());
        editedInstructor.setEmail(instructor.getEmail());
        editedInstructor.makeUsername(instructor.getEmail());

        userRepo.save(editedInstructor);

        return editedInstructor;
    }

}
