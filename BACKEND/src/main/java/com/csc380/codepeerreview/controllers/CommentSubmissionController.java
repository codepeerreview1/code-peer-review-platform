package com.csc380.codepeerreview.controllers;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import com.csc380.codepeerreview.models.CommentSubmission;
import com.csc380.codepeerreview.models.Post;
import com.csc380.codepeerreview.repositories.CommentSubmissionRepository;
import com.csc380.codepeerreview.repositories.PostRepository;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CommentSubmissionController {

    // used to work a repository with a controller
    @Autowired
    public CommentSubmissionRepository commentSubmissionRepository;

    @Autowired
    public PostRepository postRepository;

    // List all comment submissions
    @GetMapping(value = "/comments")
    public List<CommentSubmission> getAllCommentSubmissions() {
        List<CommentSubmission> comments = commentSubmissionRepository.findAll(); // order printed by most recent
        Collections.reverse(comments);
        return comments;
    }

    // List comment submissions by particular criteria // By user

    @GetMapping(path = "/comments/users/{user}")
    public List<CommentSubmission> getCommentSubmissionsByUsername(@PathVariable("user") String user) {
        List<CommentSubmission> commentSubmissions = commentSubmissionRepository.findByUser(user);
        Collections.reverse(commentSubmissions);
        return commentSubmissions;
    }

    @GetMapping(path = "/comments/post/{url}")
    public List<CommentSubmission> getCommentSubmissionsById(@PathVariable("url") String url) {

        List<CommentSubmission> commentSubmissions = commentSubmissionRepository.findByUrl(url);

        Collections.reverse(commentSubmissions);

        return commentSubmissions;
    }

    // By flagged
    @GetMapping(path = "/comments/flagged/{flag}")
    public List<CommentSubmission> getCommentSubmissionsByFlagged(@PathVariable("flag") String boolStr) {

        List<CommentSubmission> commentSubmissions;
        if (boolStr.equalsIgnoreCase("false")) {
            commentSubmissions = commentSubmissionRepository.findByFlagged(false);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;

        } else {
            commentSubmissions = commentSubmissionRepository.findByFlagged(true);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;
        }
    }

    // By helpful
    @GetMapping(path = "/comments/helpful/{helpful}")
    public List<CommentSubmission> getCommentSubmissionsByHelpful(@PathVariable("helpful") String boolStr) {

        List<CommentSubmission> commentSubmissions;
        if (boolStr.equalsIgnoreCase("false")) {
            commentSubmissions = commentSubmissionRepository.findByHelpful(false);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;

        } else {
            commentSubmissions = commentSubmissionRepository.findByHelpful(true);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;
        }
    }

    // By Users and Helpful
    @GetMapping(path = "/comments/users/{user}/{helpful}")
    public List<CommentSubmission> getCommentSubmissionsByUsernameAndHelpful(@PathVariable("user") String user,
            @PathVariable("helpful") String boolStr) {

        List<CommentSubmission> commentSubmissions;
        if (boolStr.equalsIgnoreCase("false")) {
            commentSubmissions = commentSubmissionRepository.findByUserAndHelpful(user, false);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;

        } else {
            commentSubmissions = commentSubmissionRepository.findByUserAndHelpful(user, true);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;
        }
    }

    // By Users and Flagged
    @GetMapping(path = "/comments/users/{user}/{flag}")
    public List<CommentSubmission> getCommentSubmissionsByUsernameAndFlagged(@PathVariable("user") String user,
            @PathVariable("flag") String boolStr) {

        List<CommentSubmission> commentSubmissions;
        if (boolStr.equalsIgnoreCase("false")) {
            commentSubmissions = commentSubmissionRepository.findByUserAndFlagged(user, false);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;

        } else {
            commentSubmissions = commentSubmissionRepository.findByUserAndFlagged(user, true);
            Collections.reverse(commentSubmissions);
            return commentSubmissions;
        }
    }

    // Create a comment submission
    @PostMapping(value = "/comments/{url}")
    public String createCommentSubmission(@RequestBody CommentSubmission cS, @PathVariable("url") String url) {

        List<Post> myPost = postRepository.findByUrl(url);

        String id = myPost.get(0).getId();

        String date = LocalDateTime.now().toString();

        cS.setUrl(url);
        cS.setPost(new ObjectId(id));

        // cS.setPost(new ObjectId(id));

        cS.setDate(date);
        commentSubmissionRepository.insert(cS);

        return cS.getUser() + "'s comment submission has been added!";

    }

    // Delete all comment submissions
    @DeleteMapping(value = "/comments/deleteAllComments")
    public String deleteAllCommentSubmissions() {
        commentSubmissionRepository.deleteAll();
        return "All data deleted";
    }

    /*
     * // Delete particular comment submissions
     * 
     * @DeleteMapping(value = "/comments/{i}") public String
     * deleteCourse(@PathVariable String i) { String id = i.substring(10, 34);
     * //assuming the _id's are all the same length CommentSubmission c =
     * commentSubmissionRepository.findById(id);
     * 
     * if (c.isPresent()) { commentSubmissionRepository.deleteById(id); return
     * "Course deleted. _id = " + id; } else return
     * "Course not found. Please try again.";
     * 
     * return "hello"; }
     */

    @PutMapping(value = "/comments/{id}")
    public CommentSubmission editComment(@PathVariable ObjectId id, @RequestBody CommentSubmission comment) {
        CommentSubmission editedComment = commentSubmissionRepository.findById(id);

        editedComment.setUser(comment.getUser());
        editedComment.setLineComments(comment.getLineComments());
        editedComment.setLineNums(comment.getLineNums());
        editedComment.setDate(comment.getDate());

        return editedComment;
    }
}