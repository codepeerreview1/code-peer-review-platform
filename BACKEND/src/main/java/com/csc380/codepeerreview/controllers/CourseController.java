package com.csc380.codepeerreview.controllers;

import java.util.List;
import java.util.Optional;

import com.csc380.codepeerreview.models.Course;
import com.csc380.codepeerreview.models.Gradebook;
import com.csc380.codepeerreview.models.User;
import com.csc380.codepeerreview.repositories.CourseRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CourseController {

    // used to work a repository with a controller
    @Autowired
    public CourseRepository courseRepository;
    @Autowired
    public StudentController studentController = new StudentController();
    @Autowired
    public GradebookController gradebookController = new GradebookController();

    // List all courses
    @GetMapping(value = "/courses")
    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    // SEND A LIST OF STUDENTS TO THE GRADEBOOK AT SOME POINT IN HERE!
    // Create a Course
    @PostMapping(value = "/courses/create")
    public Course createCourse(@RequestBody String c) {

        // wariables
        Course newCourse = new Course();
        String courseID = "";
        boolean gotCourseID = false;
        c = c.replaceAll("[\\]\\}]", "").replaceAll("\\\\t", " ");
        String[] rows = c.split("\\{");
        User[] students = new User[rows.length-1];

        int i = 0;
        for (String r : rows) {

            // Slip past the [ at the beginning. Admittedly, this is kind of hard coded, but as long as the formatting is consistent, we're okay.
            if (i != 0) {

                User newStudent = new User();

                String[] elements = r.split(",");

                int j = 0;
                for (String e : elements) {

                    // Read these values and parse out what we want
                    String value = e.substring(e.indexOf(":") + 1);
                    value = value.replace("\"", "");

                    // Last Name
                    if (j == 0)
                        newStudent.setNameLast(value);
                    // First Name
                    if (j == 1)
                        newStudent.setNameFirst(value);
                    // CourseID
                    if (j == 5) {
                        newStudent.setCourse(value);
                        if (!gotCourseID) {
                            courseID = value;
                            gotCourseID = true;
                        }
                    }
                    // Email
                    if (j == 6)
                        newStudent.setEmail(value);

                    // iterate to next element
                    j++;
                }

                // book keeping
                students[i-1] = newStudent;
                studentController.createStudent(newStudent);
            }
            i++;
        }

        // Make a gradebook for this course
        Gradebook g = gradebookController.createGradebook(students);

        newCourse.setStudents(students);
        newCourse.setGradebookID(g.getId());
        newCourse.setName(courseID);
        // add professor and TA emails???
        courseRepository.insert(newCourse);
        return newCourse;

    }

    // Delete all courses
    @DeleteMapping(value = "/courses/deleteAllCourses")
    public String deleteAllCourses() {
        courseRepository.deleteAll();
        return "All data deleted";
    }

    // Delete particular Courses by id
    @DeleteMapping(value = "/courses/{i}")
    public String deleteCourse(@PathVariable String i) {
        String id = i.substring(10, 34); // assuming the _id's are all the same length
        Optional<Course> c = courseRepository.findById(id);
        if (c.isPresent()) {
            courseRepository.deleteById(id);
            return "Course deleted. _id = " + id;
        } else
            return "Course not found. Please try again.";
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public ResourceNotFoundException() {
        }
    }

}