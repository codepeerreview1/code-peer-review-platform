package com.csc380.codepeerreview.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ErrorController {

    @GetMapping("/error")
    public String error() {
        return "{\"success\": false}";
    }

}
