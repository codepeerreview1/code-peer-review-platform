package com.csc380.codepeerreview.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.io.IOException;
import java.time.LocalDateTime;

import com.csc380.codepeerreview.lexiconMaterials.CurseWordChecker;
import com.csc380.codepeerreview.models.CommentSubmission;
import com.csc380.codepeerreview.models.Post;
import com.csc380.codepeerreview.models.User;
import com.csc380.codepeerreview.repositories.CommentSubmissionRepository;
import com.csc380.codepeerreview.repositories.PostRepository;
import com.csc380.codepeerreview.repositories.UserRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class PostController {

    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private CurseWordChecker cwc = new CurseWordChecker();

    @Autowired
    public PostRepository postRepository;

    @Autowired
    public CommentSubmissionRepository commentRepo;

    @Autowired
    public UserRepository userRepo;

    @GetMapping(value = "/posts")
    public List<Post> getAllPosts(@RequestBody String credentials) {

        JsonObject jsonObj = new JsonObject();

        List<Post> posts = postRepository.findAll();
        Collections.reverse(posts);

        return posts;
    }

    @GetMapping(path = "/posts/flagged/{flag}")
    public List<Post> getPostsByFlagged(@PathVariable("flag") String boolStr) {

        List<Post> posts;
        if (boolStr.equalsIgnoreCase("false")) {
            posts = postRepository.findByFlagged(false);
            Collections.reverse(posts);
            return posts;

        } else {
            posts = postRepository.findByFlagged(true);
            Collections.reverse(posts);
            return posts;
        }

    }

    @GetMapping(path = "/posts/id/{url}")
    public List<Post> getPostsById(@PathVariable("url") String url) {
        String json = "";
        JsonElement jsonElement;
        JsonObject jsonObj = new JsonObject();
        Post myPost = new Post();
        boolean success = false;
        List<Post> posts;

        // ObjectId mongoId = new ObjectId(id);
        posts = postRepository.findByUrl(url);
        return posts;

        /*
         * // Get Comments List<CommentSubmission> comments = commentRepo.findByPost(new
         * ObjectId(id));
         * 
         * for (CommentSubmission c : comments) { c.setStrId(c.getId()); }
         * 
         * 
         * jsonElement = new JsonParser().parse(json); jsonObj.add("Post", jsonElement);
         * System.out.println(jsonObj.toString());
         * 
         * 
         * String commentsJson = gson.toJson(comments); // converts to json jsonElement
         * = new JsonParser().parse(commentsJson);
         * System.out.println(jsonElement.toString());
         * 
         * jsonObj.add("Comments", jsonElement); System.out.println(jsonObj.toString());
         */

    }

    @PostMapping("/posts/create")
    public String createPost(@RequestBody String p) throws IOException {

        // The post we plan to add
        Post newPost = new Post();

        // Substrings to parse the elements that we want, plain and simple with some
        // character addition
        String title = p.substring(p.indexOf("\"title\":") + 9, p.indexOf("\"content\":") - 2);
        String content = p.substring(p.indexOf("\"content\":") + 11, p.indexOf("\"code\":") - 2);
        String possibleCode = p.substring(p.indexOf("\"code\":") + 8, p.indexOf("\"file\":") - 2);
        String possibleFileCode = p.substring(p.indexOf("\"file\":") + 8, p.indexOf("\"user\":") - 2);
        String user = p.substring(p.indexOf("\"user\":") + 8, p.indexOf("}", p.length() - 4) - 1);

        // Language check before proceeding. Make sure it's ALL good. :D
        if (cwc.curseCheckOnString(title) && cwc.curseCheckOnString(content) && cwc.curseCheckOnString(possibleCode)
                && cwc.curseCheckOnString(possibleFileCode) && cwc.curseCheckOnString(user)) {
            // Didn't fail curse check. let's keep going

            // Get some helpful strings
            String date = LocalDateTime.now().toString();
            System.out.println(date);
            Random rand = new Random();
            int randomNum = rand.nextInt(1000000000) + 1000000;
            String postUrl = user + "_" + title.replace(' ', '_');
            String dateString = date.substring(0, 10);
            postUrl += dateString + "_" + randomNum;
            String userName = user.substring(0, user.indexOf("@"));

            // Set the fields for this post object
            newPost.setTitle(title);
            newPost.setContent(content);
            newPost.setUser(userName);
            newPost.setUrl(postUrl);
            newPost.setDate(date);
            // What's the difference between flagged and reported???
            newPost.setFlagged(false);
            newPost.setReported(false);

            // Set the code
            // NOTE: File upload code OVERWRITES text based code
            if (!possibleFileCode.equals("")) {
                String input = possibleFileCode.replaceAll("(\\\\r\\\\n|\\\\n)", "\n");
                input = input.replaceAll("(\\\\\")", "\"");
                input = input.replaceAll("(\\\\\\\\)", "\\\\");
                newPost.setCode(input);
            } else if (!possibleCode.equals("")) {
                String input = possibleCode.replaceAll("(\\\\r\\\\n|\\\\n)", "\n");
                input = input.replaceAll("(\\\\\")", "\"");
                input = input.replaceAll("(\\\\\\\\)", "\\\\");
                newPost.setCode(input);
            } else {
                newPost.setCode("NO CODE WAS SUBMITTED FOR THIS POST!");
            }

            // add this post to the repository
            postRepository.insert(newPost);
            return newPost.getId();
        } else
            return "Failed langauge check. Please refrain from using innapropriate language on this platform.";
    }

    @PutMapping("/posts/report/{id}")
    public Post reportPost(@PathVariable("id") String id) {

        Post reportedPost = postRepository.findById(new ObjectId(id));

        reportedPost.setReported(true);

        postRepository.save(reportedPost);

        return reportedPost;
    }

    @PutMapping("/posts/restore/{id}")
    public Post restorePost(@PathVariable("id") String id) {

        Post reportedPost = postRepository.findById(new ObjectId(id));

        reportedPost.setReported(false);

        postRepository.save(reportedPost);

        return reportedPost;
    }

    @DeleteMapping("posts/id/{id}")
    public Post deletePost(@PathVariable("id") String id) {

        Post deletePost = postRepository.deleteById(new ObjectId(id));
        return deletePost;

    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public ResourceNotFoundException() {

        }
    }

    @PutMapping(value = "/posts/{id}")
    public Post editPost(@PathVariable ObjectId id, @RequestBody Post post) {
        Post editedPost = postRepository.findById(id);

        editedPost.setUser(post.getUser());
        editedPost.setTitle(post.getTitle());
        editedPost.setContent(post.getContent());
        editedPost.setDate(post.getDate());

        return editedPost;
    }
}
