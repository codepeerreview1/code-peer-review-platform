package com.csc380.codepeerreview.lexiconMaterials;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CurseWordChecker {

    public boolean curseCheckOnString(String words) throws IOException {

        // Get the lexicon file
        File lexicon = new File("backend\\src\\main\\java\\com\\csc380\\codepeerreview\\lexiconMaterials\\CurseWordLexicon.txt");

        // Lists of words from each file
        List<String> text = readWordString(words);
        List<String> lex = readFileLines(lexicon);

        return curseWordCheck(lex, text);
    }

    // For lexicon file
    public static List<String> readFileLines(File f) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(f.getAbsolutePath()));
        String str;

        List<String> list = new ArrayList<String>();
        while ((str = in.readLine()) != null) {
            list.add(str);
        }
        in.close();
        return list;
    }

    public List<String> readWordString(String str) throws IOException {

        List<String> list = new ArrayList<String>();
        // get rid of uneccesary garbo
        String[] words = str.split("[^A-Za-z$@301]+");
        for (String w : words) {
            // get rid of any empty lines
            if (!w.equals(""))
                //add this word to our list
                list.add(w.toLowerCase());
        }
        return list;
    }

    // check the validity by returning a bool
    static boolean curseWordCheck(List<String> lex, List<String> words) {

        // boolean for checking process
        Boolean safe = true;

        // loop through the curse words
        for (String curseWord : lex) {
            // and see if the text contains it. If so, break and return the flag
            if (words.contains(curseWord)) {
                safe = false;
                break;
            }
        }
        return safe;
    }
}