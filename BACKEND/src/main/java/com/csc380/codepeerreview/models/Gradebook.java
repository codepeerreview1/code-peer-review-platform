package com.csc380.codepeerreview.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@Document(collection = "Gradebooks")
public class Gradebook {
    

    // Wariables
    @Id
    private ObjectId id;
    private HashMap<String, String> grades= new HashMap<String, String> ();
    private String courseID;
    private User[] students;

    public Gradebook() {
    }

    public Gradebook(HashMap<String, String> grades, String courseID, User[] students) {
        this.grades = grades;
        this.courseID = courseID;
        this.students = students;
    }

    public void enterGrade(String name, String grade) {
        grades.put(name,grade);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getCourseID() {
        return courseID;
    }

    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    public User[] getStudents() {
        return students;
    }

    public void setStudents(User[] students) {
        this.students = students;
    }

    public HashMap<String, String> getGrades() {
        return grades;
    }

    public void setGrades(HashMap<String, String> grades) {
        this.grades = grades;
    }

    
    
}
