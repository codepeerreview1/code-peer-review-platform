package com.csc380.codepeerreview.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Posts")
public class Post {

    @Id
    private ObjectId id;
    private String user;
    private String title;
    private String content;
    private String date;
    private String code;
    private boolean flagged;
    private String url;
    private String strId;
    private boolean reported;

    public Post() {
        super();
    }

    public Post(ObjectId id, String user, String title, String content, String date, boolean flagged, String code,
            String url, boolean reported) {
        this.id = id;
        this.user = user;
        this.title = title;
        this.content = content;
        this.date = date;
        this.code = code;
        this.flagged = flagged;
        this.url = url;
        this.reported = reported;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean getFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String id) {
        this.strId = id;

    }

    public boolean getReported() {
        return reported;
    }

    public void setReported(boolean reported) {
        this.reported = reported;
    }

}
