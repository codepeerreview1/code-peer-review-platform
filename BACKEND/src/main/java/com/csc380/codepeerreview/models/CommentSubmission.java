package com.csc380.codepeerreview.models;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Comments")
public class CommentSubmission {

    // wariables
    @Id
    private ObjectId id;
    private String user;
    private int lineNums;
    private String lineComments;
    private String url;
    private ObjectId post;
    private boolean flagged;
    private boolean helpful;
    private String date;

    private String strId;

    // Constructor
    public CommentSubmission(ObjectId id, String user, String date, int lineNums, String lineComments, ObjectId post) {
        super();
        this.id = id;
        this.user = user;
        this.date = date;
        this.lineNums = lineNums;
        this.lineComments = lineComments;
        this.post = post;
        flagged = false;
        helpful = false;
    }

    // Constructor
    public CommentSubmission() {
        super();
    }

    // Getters and Setters

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getLineNums() {
        return lineNums;
    }

    public void setLineNums(int lineNums) {
        this.lineNums = lineNums;
    }

    public String getLineComments() {
        return lineComments;
    }

    public void setLineComments(String lineComments) {
        this.lineComments = lineComments;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFlagged() {
        return flagged;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public void setPost(ObjectId id) {
        this.post = id;
    }

    public String getPost() {

        return post.toHexString();

    }

    public String getStrId() {
        return strId;
    }

    public void setStrId(String id) {
        this.strId = id;

    }

    public boolean isHelpful() {
        return helpful;
    }

    public void setHelpful(boolean helpful) {
        this.helpful = helpful;
    }

}