
/*
 * package com.csc380.codepeerreview.models;
 * 
 * import org.bson.types.ObjectId; import
 * org.springframework.data.annotation.Id; import
 * org.springframework.data.mongodb.core.mapping.Document;
 * 
 * @Document(collection = "Authentication") public class Credentials {
 * 
 * @Id private ObjectId id; private String email; private String userId; private
 * String message;
 * 
 * public Credentials() { super(); }
 * 
 * public Credentials(ObjectId id, String email, String userId, String message)
 * { this.id = id; this.email = email; this.userId = userId; }
 * 
 * public Credentials(String email, String userId, String message) { this.email
 * = email; this.userId = userId; }
 * 
 * public String getId() { return id.toHexString(); }
 * 
 * public void setId(ObjectId id) { this.id = id; }
 * 
 * public String getEmail() { return email; }
 * 
 * public void setEmail(String email) { this.email = email; }
 * 
 * public String getUserId() { return userId; }
 * 
 * public void setUserId(String userId) { this.userId = userId; }
 * 
 * public String getMessage() { return message; }
 * 
 * public void setMessage(String message) { this.message = message; } }
 * 
 */
