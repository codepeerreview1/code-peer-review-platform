package com.csc380.codepeerreview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodePeerReview {

	public static void main(String[] args) {
		SpringApplication.run(CodePeerReview.class, args);
	}

}